//
//  Constant.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//


#import "Constants.h"

NSString * const kOAuth2AuthorizationURL                    = @"https://api.instagram.com/oauth/authorize";
NSString * const kOAuth2TokenURL                            = @"https://api.instagram.com/oauth/access_token";

NSString * const kOAuth2ClientId                            = @"5ff59d9c840f48658aa90a034aa0927b";
NSString * const kOAuth2ClientSecret                        = @"13b4ca188ab840ed882f7ae5837cb834";
NSString * const kOAuth2RedirectURL                         = @"ig5ff59d9c840f48658aa90a034aa0927b://authorize";

NSString * const kOAuth2Scope                               = @"comments+relationships+likes";

NSString * const kBaseURL                                   = @"https://instagram.com/";
NSString * const kOAuth2ApiURL                              = @"https://api.instagram.com/v1/";


NSString * const kOAuth2Endpoint_users_self                 = @"https://api.instagram.com/v1/users/self";
NSString * const kOAuth2Endpoint_users_self_feed            = @"https://api.instagram.com/v1/users/self/feed";
NSString * const kOAuth2Endpoint_users_self_popular         = @"https://api.instagram.com/v1/media/popular";


//

NSString * const kUser                                      = @"user";
NSString * const kUsername                                  = @"username";
NSString * const kProfilePicture                            = @"profile_picture";