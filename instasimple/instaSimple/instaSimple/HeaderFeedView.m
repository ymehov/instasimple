//
//  HeaderFeedView.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "HeaderFeedView.h"
#import "UIColor+InstagramColor.h"
#import "CacheStoreManager.h"

NSInteger const kHeightOfHeaderView = 40;

@interface HeaderFeedView()

@end

@implementation HeaderFeedView

+(HeaderFeedView *) headerWithName:(NSString *)name Location:(NSString *)location ProfileImage:(NSString *)profileURL {
    NSInteger width = [UIScreen mainScreen].bounds.size.width;
    HeaderFeedView *headerView = [[HeaderFeedView alloc] initWithFrame:CGRectMake(0, 0, width, kHeightOfHeaderView)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // add username
    UILabel *labelName = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, width-50, kHeightOfHeaderView)];
    labelName.text = [NSString stringWithFormat:@"%@",name];
    labelName.textColor = [UIColor colorForInstagram];
    [headerView addSubview:labelName];
    
    // add location
    NSString *locationName = location;
    if(locationName) {
        UILabel *labelLocation = [[UILabel alloc] initWithFrame:CGRectMake(50, 20, width-50, kHeightOfHeaderView-20)];
        labelLocation.text = [NSString stringWithFormat:@"%@",locationName];
        labelLocation.textColor = [[UIColor colorForInstagram] colorWithAlphaComponent:0.9];
        [headerView addSubview:labelLocation];
        labelName.frame = CGRectMake(50, 0, width-50, kHeightOfHeaderView-20);
    }
    
    // add avatarka
    UIImageView *profileImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 30, 30)];
    profileImgView.layer.cornerRadius = profileImgView.frame.size.height/2;
    profileImgView.layer.masksToBounds = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSData *data;
        data = [[CacheStoreManager sharedManager] imageDataForKey:profileURL];
        if(!data) {
            data = [NSData dataWithContentsOfURL:[NSURL URLWithString:profileURL]];
        }
        UIImage *imageFromURL = [UIImage imageWithData:data];
        if (imageFromURL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                // Update  UI
                profileImgView.image = imageFromURL;
                [[CacheStoreManager sharedManager] storeImageData:data ForKey:profileURL];
            });
        }
    });
    [headerView addSubview:profileImgView];
    
    
    return headerView;
}


@end
