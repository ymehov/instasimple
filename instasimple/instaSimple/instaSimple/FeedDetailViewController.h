#import <UIKit/UIKit.h>

#import "MediaData.h"
#import <MessageUI/MessageUI.h>

typedef NS_ENUM(NSUInteger, CurrentStateDetailViewController) {
    stateShowingComments,
    stateWritingComments
};

@interface FeedDetailViewController : UIViewController <UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@property (assign, nonatomic)   CurrentStateDetailViewController currentState;

@property (strong, nonatomic)   MediaData *mediaData;
@property (weak, nonatomic)     IBOutlet UITextView *commentsTextView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldView;

@end
