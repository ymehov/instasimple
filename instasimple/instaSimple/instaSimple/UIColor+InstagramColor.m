//
//  UILabel+InstagramColor.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "UIColor+InstagramColor.h"

@implementation UIColor(InstagramColor)

+ (UIColor *)colorForInstagram {
    return [UIColor colorWithRed:63/255. green:114/255. blue:155/255. alpha:1];
}

@end
