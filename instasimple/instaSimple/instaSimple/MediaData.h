//
//  MediaData.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaData : NSObject

@property (nonatomic, copy)   NSString *idFeed;

@property (nonatomic, copy)   NSString *username;
@property (nonatomic, copy)   NSString *profileImageUrl;

@property (nonatomic, copy)   NSString *imageLowResUrl;
@property (nonatomic, copy)   NSString *imageThumbnailUrl;
@property (nonatomic, copy)   NSString *imageStandardUrl;
@property (nonatomic, assign) NSUInteger imageStandardSize;
@property (nonatomic, copy)   NSString *caption;

@property (nonatomic, assign) NSUInteger likes;

@property (nonatomic, assign) NSUInteger commentsNo;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, copy)   NSAttributedString *commentsStringLast;
@property (nonatomic, copy)   NSAttributedString *commentsString;

@property (nonatomic, strong) NSMutableDictionary *location;
@property (nonatomic, copy)   NSString *locationName;
@property (nonatomic, assign) NSNumber *locationLatitude;
@property (nonatomic, assign) NSNumber *locationLongitude;


- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
