//
//  AccountManager.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountManager : NSObject

+ (void)logout;

@end
