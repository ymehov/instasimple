//
//  Constant.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kOAuth2AuthorizationURL;
extern NSString * const kOAuth2TokenURL;

extern NSString * const kOAuth2ClientId;
extern NSString * const kOAuth2ClientSecret;
extern NSString * const kOAuth2RedirectURL;

extern NSString * const kBaseURL;
extern NSString * const kOAuth2ApiURL;
extern NSString * const kOAuth2Scope;
extern NSString * const kOAuth2Endpoint_users_self_feed;
extern NSString * const kOAuth2Endpoint_users_self_popular;


// mantle

extern NSString * const kUser;
extern NSString * const kUsername;
extern NSString * const kProfilePicture;
