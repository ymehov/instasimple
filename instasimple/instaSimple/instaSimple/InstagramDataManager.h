//
//  InstagramDataManager.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramDataManager : NSObject


// token OAUTH protocol
@property (copy, nonatomic) NSString *accessToken;


+ (InstagramDataManager *)sharedData;

- (NSArray *)loadRequestForEndpoint:(NSString*)endpoint;

@end
