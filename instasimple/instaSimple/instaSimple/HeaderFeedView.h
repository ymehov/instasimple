//
//  HeaderFeedView.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderFeedView : UIView

extern NSInteger const kHeightOfHeaderView;

+(HeaderFeedView *) headerWithName:(NSString *)name Location:(NSString *)location ProfileImage:(NSString *)profileURL;

@end
