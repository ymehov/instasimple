//
//  CacheImageStoreManager.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CacheStoreManager : NSObject

+ (CacheStoreManager *)sharedManager;

// image data
- (void)storeImageData:(NSData *) object ForKey: (NSString *) key;

- (NSData *)imageDataForKey: (NSString *) key;

// store json

- (void)storeMediaFeed:(NSArray *)feed;

- (NSArray *)mediaFeedFromStorage;

@end
