//
//  CacheImageStoreManager.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "CacheStoreManager.h"

NSString * const kCachePrefix = @"com.ym.instaCacheStore";
NSString * const kCacheFeed = @"feed.plist";

@interface CacheStoreManager() {
    // Create path.
    NSArray *paths;
}

@property (strong, nonatomic) dispatch_queue_t queue;

@property (nonatomic, strong) NSCache *cacheStorage;

@end

@implementation CacheStoreManager

+ (CacheStoreManager *)sharedManager {
    static CacheStoreManager * _sharedData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedData = [[self alloc] init];
        
    });
    return _sharedData;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.cacheStorage = [[NSCache alloc] init];
        
        // Create path.
        paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        
        //queue
        _queue = dispatch_queue_create([kCachePrefix UTF8String], DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}
#pragma mark - image store

- (void)storeImageData:(NSData *) object ForKey: (NSString *) key {
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@""];
    [self.cacheStorage setObject:object forKey:key];
    
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:key];
    
    // Save image.
    dispatch_async(_queue, ^{
        // https:/scontent.cdninstagram.com/hphotos-xaf1/t51.2885-15/e15/11355966_869278589831299_492617656_n.jpg
        NSError *err;
        [object writeToFile:filePath options:NSDataWritingAtomic error:&err];
        if(!err) {
            NSLog(@"could not store - %@",err.description);
        }
    });
}

- (NSData *)imageDataForKey: (NSString *) key {
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSData *retValue = [self.cacheStorage objectForKey:key];
    if(!retValue) {
        retValue = [NSData dataWithContentsOfFile:[[paths objectAtIndex:0] stringByAppendingPathComponent:key]];
    }
    return retValue;
}

#pragma mark - feed store

- (void)storeMediaFeed:(NSArray *)feed {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:feed];
    if(![data writeToFile:[[paths objectAtIndex:0] stringByAppendingPathComponent:kCacheFeed] atomically:YES]) {
        NSLog(@"could not save to disk");
    }
}

- (NSArray *)mediaFeedFromStorage {
    NSData *data = [NSData dataWithContentsOfFile:[[paths objectAtIndex:0] stringByAppendingPathComponent:kCacheFeed]];
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

@end
