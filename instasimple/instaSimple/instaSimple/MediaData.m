//
//  MediaData.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "MediaData.h"
#import "UIColor+InstagramColor.h"
#import "Constants.h"
#import <UIKit/UIKit.h>

@implementation MediaData

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    if (self = [super init]) {
        
        
        _idFeed                     = [attributes valueForKeyPath:@"id"];
        
        _username                    = [[attributes valueForKeyPath:kUser]    valueForKeyPath:kUsername];
        _profileImageUrl             = [[attributes valueForKeyPath:kUser]    valueForKeyPath:kProfilePicture];
        
        _imageLowResUrl              = [[[attributes valueForKeyPath:@"images"]  valueForKeyPath:@"low_resolution"]      valueForKeyPath:@"url"];
        _imageThumbnailUrl           = [[[attributes valueForKeyPath:@"images"]  valueForKeyPath:@"thumbnail"]           valueForKeyPath:@"url"];
        _imageStandardUrl            = [[[attributes valueForKeyPath:@"images"]  valueForKeyPath:@"standard_resolution"] valueForKeyPath:@"url"];
        _imageStandardSize           = [[[[attributes valueForKeyPath:@"images"]  valueForKeyPath:@"standard_resolution"] valueForKeyPath:@"width"]integerValue];
        
        _caption                = [[attributes valueForKeyPath:@"caption"]  valueForKeyPath:@"text"];
        // Check if there is a caption
        if ( ([_caption  isEqual: @""]) || ([[attributes valueForKeyPath:@"caption"]  valueForKeyPath:@"text"] == [NSNull null]) )
            _caption = nil;
        
        _likes                  = [[[attributes objectForKey:@"likes"]      valueForKey:@"count"]                   integerValue];
        
        _commentsNo             = [[[attributes objectForKey:@"comments"]   valueForKey:@"count"]                   integerValue];
        _comments               = [[attributes valueForKeyPath:@"comments"] valueForKeyPath:@"data"];
        if (_commentsNo>0) {
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] init];
            for (NSInteger i = 0;  i < _comments.count; i++) {
                NSString *fName = [[_comments[i] valueForKey:@"from"] valueForKeyPath:@"full_name"];
                NSString *comment = [_comments[i] valueForKeyPath:@"text"];
                
                // generate attributed string
                NSDictionary * attributes = [NSDictionary dictionaryWithObject:[UIColor colorForInstagram] forKey:NSForegroundColorAttributeName];
                NSAttributedString * subString = [[NSAttributedString alloc] initWithString:fName attributes:attributes];
                [string appendAttributedString:subString];
                [string appendAttributedString:[[NSAttributedString alloc] initWithString: @" "]];
                
                attributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
                subString = [[NSAttributedString alloc] initWithString:comment attributes:attributes];
                [string appendAttributedString:subString];
                [string appendAttributedString:[[NSAttributedString alloc] initWithString: @"\n"]];
            }
            _commentsString   = string;
        }
        
        NSArray *lastComments;
        if(_comments.count >= 3) {
            lastComments = [_comments subarrayWithRange:NSMakeRange(_comments.count-3,3)];
        }
        else lastComments = _comments;
        if (_commentsNo>0) {
            NSMutableAttributedString * string = [[NSMutableAttributedString alloc] init];
            for (NSInteger i = 0; i < 3 && i < lastComments.count; i++) {
                NSString *fName = [[lastComments[i] valueForKey:@"from"] valueForKeyPath:@"full_name"];
                NSString *comment = [lastComments[i] valueForKeyPath:@"text"];
                
                // generate attributed string
                NSDictionary * attributes = [NSDictionary dictionaryWithObject:[UIColor colorForInstagram] forKey:NSForegroundColorAttributeName];
                NSAttributedString * subString = [[NSAttributedString alloc] initWithString:fName attributes:attributes];
                [string appendAttributedString:subString];
                [string appendAttributedString:[[NSAttributedString alloc] initWithString: @" "]];
                
                attributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
                subString = [[NSAttributedString alloc] initWithString:comment attributes:attributes];
                [string appendAttributedString:subString];
                [string appendAttributedString:[[NSAttributedString alloc] initWithString: @"\n"]];
            }
            _commentsStringLast   = string;
        }
        else _comments = nil;
        
        
        _location               = [attributes valueForKeyPath:@"location"];
        // Check if there are location values
        if ([_location valueForKeyPath:@"latitude"]  == [NSNull null]
            || [_location valueForKeyPath:@"longitude"]  == [NSNull null]) {
            _location = nil;
        } else {
            _locationLatitude  = [_location valueForKeyPath:@"latitude"];
            _locationLongitude = [_location valueForKeyPath:@"longitude"];
            if ([_location valueForKeyPath:@"name"] != [NSNull null]) _locationName = [_location valueForKeyPath:@"name"];
        }
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.idFeed forKey:@"idFeed"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.profileImageUrl forKey:@"profileImageUrl"];
    [encoder encodeObject:self.imageStandardUrl forKey:@"imageStandardUrl"];
    [encoder encodeObject:self.caption forKey:@"caption"];
    [encoder encodeObject:self.comments forKey:@"comments"];
    [encoder encodeObject:@(self.commentsNo) forKey:@"commentsNo"];
    [encoder encodeObject:self.commentsStringLast forKey:@"commentsStringLast"];
    [encoder encodeObject:self.commentsString forKey:@"commentsString"];
    [encoder encodeObject:self.locationName forKey:@"locationName"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.idFeed = [decoder decodeObjectForKey:@"idFeed"];
        self.username = [decoder decodeObjectForKey:@"username"];
        self.profileImageUrl = [decoder decodeObjectForKey:@"profileImageUrl"];
        self.imageStandardUrl = [decoder decodeObjectForKey:@"imageStandardUrl"];
        self.caption = [decoder decodeObjectForKey:@"caption"];
        self.comments = [decoder decodeObjectForKey:@"comments"];
        self.commentsNo = [[decoder decodeObjectForKey:@"commentsNo"] intValue];
        self.commentsStringLast = [decoder decodeObjectForKey:@"commentsStringLast"];
        self.locationName = [decoder decodeObjectForKey:@"locationName"];
    }
    return self;
}


@end
