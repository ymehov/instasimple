#import "FeedDetailViewController.h"


#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "CacheStoreManager.h"

@interface FeedDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@end



@implementation FeedDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // in future will be replaced to localizeString
    //self.title = @"Комментарии";
    
    
    
    NSData * imgData = [[CacheStoreManager sharedManager ] imageDataForKey:_mediaData.imageStandardUrl];
    self.imageView.image = [UIImage imageWithData:imgData];
    
    self.scrollView.contentSize = CGSizeMake(_imageView.frame.size.width, _imageView.frame.size.height);
    self.scrollView.maximumZoomScale = 4.0;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.delegate = self;
    
    self.commentsTextView.attributedText = self.mediaData.commentsString;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardUp:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDown:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.currentState == stateWritingComments ) {
        [self.textFieldView becomeFirstResponder];
    }
}

#pragma mark - TextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    [UIPasteboard generalPasteboard].string = textField.text;
    UIAlertView *alertInfo = [[UIAlertView alloc] initWithTitle:@"" message:@"Ваше сообщение скопировано в память. Сейчас Вы будете перенаправлены в приложение Instagram" delegate:self cancelButtonTitle:@"ОК" otherButtonTitles:nil];
    [alertInfo show];
    
    return YES;
}

- (void)keyboardUp:(NSNotification *)notification {
    NSDictionary *info = [notification userInfo];
    CGRect keyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    UIEdgeInsets contentInset = self.scrollView.contentInset;
    contentInset.bottom = keyboardRect.size.height;
    self.scrollView.contentInset = contentInset;
}

- (void)keyboardDown:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // в настоящий момент нельзя отправить комент, поэтому лайфхак
    NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://media?id=%@",self.mediaData.idFeed]];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
