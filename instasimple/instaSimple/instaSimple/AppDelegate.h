//
//  AppDelegate.h
//  instaSimple
//
//  Created by iLego on 25.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderClasses.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

