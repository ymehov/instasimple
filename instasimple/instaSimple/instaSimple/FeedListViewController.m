#import "FeedListViewController.h"

#import "InstagramDataManager.h"
#import "MediaData.h"
#import "FeedTableViewCell.h"
#import "FeedDetailViewController.h"
#import "HeaderFeedView.h"
#import "Constants.h"
#import "NavigationBarAnimator.h"
#import "CacheStoreManager.h"
#import "AccountManager.h"


@interface FeedListViewController () {
    dispatch_queue_t imageQueue;
    dispatch_queue_t imageProfileQueue;
}

@property (nonatomic, strong) NSArray *dataArray ;

@property (nonatomic, strong) CacheStoreManager *cacheImages;
@property (nonatomic, strong) NSMutableDictionary *cachedHeights;


@end



@implementation FeedListViewController

- (void)loadDataArray {
    self.dataArray = [[InstagramDataManager sharedData] loadRequestForEndpoint:kOAuth2Endpoint_users_self_popular];
    [self.tableView reloadData];
    [self.refreshControl performSelector:@selector(endRefreshing) withObject:nil afterDelay:0.05];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // load all data
    self.dataArray = [[InstagramDataManager sharedData] loadRequestForEndpoint:kOAuth2Endpoint_users_self_popular];
    
    // cache
    self.cacheImages = [CacheStoreManager sharedManager];
    self.cachedHeights = [NSMutableDictionary dictionaryWithCapacity:self.dataArray.count];
    
    // pull to refresh control
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.refreshControl addTarget:self action:@selector(refreshFeed) forControlEvents:UIControlEventValueChanged];
    
    // create queue for loading images
    imageQueue = dispatch_queue_create("Image Loader",NULL);
    imageProfileQueue = dispatch_queue_create("Image Profile Loader",NULL);
    
    // nav bar options
    self.navigationItem.hidesBackButton = YES;
    UIImageView *instHeader = [[UIImageView alloc] initWithFrame:CGRectMake(50, 0, CGRectGetWidth(self.view.frame)-100, CGRectGetHeight(self.navigationController.navigationBar.frame))];
    instHeader.contentMode = UIViewContentModeScaleAspectFit;
    instHeader.backgroundColor = [UIColor clearColor];
    instHeader.image = [UIImage imageNamed:@"instNavBar"];
    [self.navigationController.navigationBar addSubview:instHeader];
    self.navigationController.scrollNavigationBar.scrollView = self.tableView;
    
    UIImage *faceImage = [UIImage imageNamed:@"logout"];
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutBtn.bounds = CGRectMake( 0, 0, 30, 30 );
    [logoutBtn setImage:faceImage forState:UIControlStateNormal];
    UIBarButtonItem *logout = [[UIBarButtonItem alloc] initWithCustomView:logoutBtn];
    [logoutBtn addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = logout;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    MediaData * imageData = self.dataArray[section];
    return [HeaderFeedView headerWithName:imageData.username Location:imageData.locationName ProfileImage:imageData.profileImageUrl];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kHeightOfHeaderView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = NSStringFromClass([FeedTableViewCell class]);
    FeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self configureBasicCell:cell atIndexPath:indexPath];
    
    
    // TODO: save Images on Device for faster loading, offline use ?
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ((NSNumber*)[self.cachedHeights objectForKey:indexPath]).floatValue
                                ?:[self heightForBasicCellAtIndexPath:indexPath];
}


- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = NSStringFromClass([FeedTableViewCell class]);
    static FeedTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    });
    MediaData * imageData = self.dataArray[indexPath.section];
    CGFloat height = 420 + [imageData.caption boundingRectWithSize:CGSizeMake(sizingCell.frame.size.width, MAXFLOAT)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{
                                                                    NSFontAttributeName : sizingCell.textLabel.font
                                                                    }
                                                          context:nil].size.height;
    height += [imageData.commentsStringLast.string boundingRectWithSize:CGSizeMake(sizingCell.frame.size.width, MAXFLOAT)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{
                                                       NSFontAttributeName : sizingCell.textLabel.font
                                                       }
                                             context:nil].size.height;
//    if(imageData.commentsNo == 0) {
//        height -= CGRectGetHeight(sizingCell.commentsButton.frame);
//    }

    [self.cachedHeights setObject:@(height) forKey:indexPath];
    return height;
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f;
}


- (void)configureBasicCell:(FeedTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    MediaData * imageData = self.dataArray[indexPath.section];
    
    
    cell.captionLabel.text = imageData.caption;
    
    cell.LikesNo.text = [NSString stringWithFormat:@"♡ Нравится: %lu",(unsigned long)imageData.likes];
    
    NSUInteger countOfComments = imageData.commentsNo;
    if(countOfComments) {
        [cell.commentsButton setTitle:[NSString stringWithFormat:@"Комментарии (%ld)",(unsigned long)countOfComments] forState:UIControlStateNormal];
    }
    else {
        [cell.commentsButton setTitle:@"" forState:UIControlStateNormal];
    }
    cell.commentsButton.tag = indexPath.section;
    cell.commentsButtonHeader.tag = indexPath.section;
    cell.mediaImage.image = [UIImage imageNamed:@"placeholder-instagram"];
    cell.lastCommentsLabel.attributedText = imageData.commentsStringLast;
    
    dispatch_async(imageQueue, ^{
        NSData * imageDataFromURL;
        if(!(imageDataFromURL = [self.cacheImages imageDataForKey:imageData.imageStandardUrl])) {
            NSURL * url = [NSURL URLWithString:imageData.imageStandardUrl];
            imageDataFromURL = [NSData dataWithContentsOfURL:url];
            if (!imageDataFromURL) return;
            
            // save to cache
            [self.cacheImages storeImageData:imageDataFromURL ForKey:imageData.imageStandardUrl];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update  UI
            NSLog(@"%@",@"1");
            cell.mediaImage.image = [UIImage imageWithData:imageDataFromURL];
        });
    });
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    [self.navigationController.scrollNavigationBar resetToDefaultPositionWithAnimation:NO];
}

#pragma mark - Refresh Control 

-(void)refreshFeed {
    [self loadDataArray];
}

#pragma mark - Storyboard

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    [self.navigationController.scrollNavigationBar resetToDefaultPositionWithAnimation:NO];
    if ([[segue identifier] isEqualToString:@"ShowMediaDetail"])
    {
        FeedDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.mediaData = self.dataArray[sender.tag];
    }
    else if ([[segue identifier] isEqualToString:@"ShowMediaDetailWithText" ]) {
        FeedDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.currentState = stateWritingComments;
        detailViewController.mediaData = self.dataArray[sender.tag];
    }
}

#pragma mark - logout

- (void)logoutAction {
    [AccountManager logout];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
