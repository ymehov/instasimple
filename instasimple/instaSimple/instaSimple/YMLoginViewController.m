//
//  ViewController.m
//  instaSimple
//
//  Created by iLego on 25.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "YMLoginViewController.h"
#import "Constants.h"
#import "InstagramDataManager.h"
#import "FeedListViewController.h"
#import "Reachability.h"

@interface YMLoginViewController () {
    Reachability *internetReachableFoo;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *webView;


@end

@implementation YMLoginViewController

// Checks if we have an internet connection or not
- (void)testInternetConnection
{
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    __weak YMLoginViewController *weakSelf = self;
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf checkLoggedIn];
        });
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf performSegueWithIdentifier:@"FeedTableView" sender:weakSelf];
        });
    };
    
    [internetReachableFoo startNotifier];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testInternetConnection];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - Login


- (void) checkLoggedIn {
    
    
    // TODO: create new class f.e. InstagramManager and move this method
    
    NSString *fullAuthUrlString = [[NSString alloc]
                                   initWithFormat:@"%@/?client_id=%@&redirect_uri=%@&scope=%@&response_type=token&display=touch",
                                   kOAuth2AuthorizationURL,
                                   kOAuth2ClientId,
                                   kOAuth2RedirectURL,
                                   kOAuth2Scope
                                   ];
    
    NSURL *url = [NSURL URLWithString:fullAuthUrlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:requestObj];
    self.webView.hidden = NO;
    self.webView.delegate = self;
    
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    NSString* urlString = [[request URL] absoluteString];
    
    if ([urlString hasPrefix:kOAuth2RedirectURL]) {
        NSRange tokenParam = [urlString rangeOfString: @"access_token="];
        if (tokenParam.location != NSNotFound) {
            NSString* token = [urlString substringFromIndex: NSMaxRange(tokenParam)];
            
            // If there are more args, don't include them in the token:
            NSRange endRange = [token rangeOfString: @"&"];
            if (endRange.location != NSNotFound)
                token = [token substringToIndex: endRange.location];
            
            NSLog(@"access token %@ %lu", token, (unsigned long)[token length]);
            if ([token length] > 0 ) {
                [InstagramDataManager sharedData].accessToken = token;
                [self performSegueWithIdentifier:@"FeedTableView" sender:self];
            }
        }
        else {
            // Handle the access rejected case here.
            NSLog(@"rejected case, user denied request");
        }
        return NO;
    }
    return YES;
}



#pragma mark - WebView delegates

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.activityIndicator startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
