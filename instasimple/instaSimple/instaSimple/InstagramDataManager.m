//
//  InstagramDataManager.m
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import "InstagramDataManager.h"
#import "CacheStoreManager.h"

#import "Constants.h"
#import "MediaData.h"

@interface InstagramDataManager ()

@end



@implementation InstagramDataManager

+ (InstagramDataManager *)sharedData
{
    static InstagramDataManager * _sharedData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedData = [[self alloc] init];
    });
    return _sharedData;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    return self;
}



- (NSArray *)loadRequestForEndpoint:(NSString *)endpoint {
    
    NSString * url = [NSString stringWithFormat:@"%@?access_token=%@",endpoint,self.accessToken];
    NSMutableArray *mediaDataArray = [[NSMutableArray alloc]init];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    if(data) {
        
        NSDictionary * dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        NSArray * responseDataArray = (NSArray*)[dictResponse objectForKey:@"data"];
        
        
        for (int i=0; i<responseDataArray.count; i++) {
            MediaData * mediaDataRef = [[MediaData alloc]initWithAttributes:responseDataArray[i]];
            [mediaDataArray addObject:mediaDataRef];
        }
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [[CacheStoreManager sharedManager] storeMediaFeed:mediaDataArray];
        });
    }
    else {
        return [[CacheStoreManager sharedManager] mediaFeedFromStorage];
    }
    
    return [mediaDataArray copy];
}


@end
