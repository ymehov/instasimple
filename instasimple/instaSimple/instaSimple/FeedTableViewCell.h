#import <UIKit/UIKit.h>
#import "DynamicPrefferedLabel.h"

@interface FeedTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *mediaImage;
@property (weak, nonatomic) IBOutlet DynamicPrefferedLabel *captionLabel;
@property (weak, nonatomic) IBOutlet DynamicPrefferedLabel *LikesNo;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UILabel *lastCommentsLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentsButtonHeader;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;

@end
