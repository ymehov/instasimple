//
//  NavigationBarAnimator.h
//  instaSimple
//
//  Created by iLego on 27.06.15.
//  Copyright (c) 2015 iLegov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ScrollNavigationBarState) {
    ScrollNavigationBarNone,
    ScrollNavigationBarScrollingDown,
    ScrollNavigationBarScrollingUp
} ;

@interface NavigationBarAnimator : UINavigationBar

@property (strong, nonatomic) UIScrollView *scrollView;
@property (assign, nonatomic) ScrollNavigationBarState scrollState;

- (void)resetToDefaultPositionWithAnimation:(BOOL)animated;

@end

@interface UINavigationController (GTScrollNavigationBarAdditions)

@property(strong, nonatomic, readonly) NavigationBarAnimator *scrollNavigationBar;

@end
